
package org.bitbucket.euler102;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provide methods for encription, decription and key finding
 */
public class Cypher {

  /**
   *  Logger for this class
   */
  static Logger logger = LoggerFactory.getLogger(Cypher.class);

  /* Decypher is the same as encryption with Xor  */
  public static byte[] decypher(String s, String key) {
    return cypher(s, key);
  }

  /**
   * Encrypt a string with a key
   *
   * @param s       The string to encrypt
   * @param key     The key
   *
   * @return        The encryption of `s` using `key`
   */
  public static byte[] cypher(String s, String key) {

    byte[] sourceString = new byte[ s.length() ];
    byte[] keyString = new byte[ key.length() ];
    byte[] encocedString = new byte[ s.length() ];
    // Xor the char with the key cyclically
    try {
      sourceString = s.getBytes("US-ASCII");
      keyString = key.getBytes("US-ASCII");

      //    Here we have to compute the result by Xoring bytes with key cyclically
      //    does not compile for some reasons.
      //    seems like xor cannot be applied to byte but they have to be converted to int
      //    and result back to byte. Check the doc for xor on int/byte
      for (int i = 0; i < s.length(); i++) {
        encocedString[ i ] = (byte)((int)sourceString[ i ] ^ (int)keyString[ i % key.length() ] );
      }

    } catch (Exception e) {
      // Log exception
      logger.error("String {} could not be cyphered:  {}", s, e.getMessage());
    }
    logger.info("Encoding is {}", encocedString);

    //  return the String that corresponds to bytes
    return encocedString;
  }

  /**
   * Given two bytes b1 and b2 find a byte such that b1 ^ b3 = b2.
   */
  private static char findKey(byte source, byte target) {

    char result = ' ';

    for (byte b = -128; b < 127; b++) {
      // If (b xor source) is equal to target, we have found the key
      if (  ( b ^ source )  == ( b ^ target ) ) {
        logger.info("Found key! {}, {}", b, (char)b);
        result = (char)b;
      }
    }
    return result;
  }

  public static String findKey(int length, String source, String target) {
    char[] keyString = {};
    try {
      byte[] byteSource = source.getBytes("US-ASCII");
      byte[] byteTarget = target.getBytes("US-ASCII");

      for(int i = 0; i <= length; i++) {
        keyString[ i ] = findKey(byteSource[ i ], byteTarget[ i ]);
      }
    }
    catch (Exception e) {
      logger.error("String {} could not be cyphered:  {}", source, e);
    }

    return new String(keyString);
  }

}
